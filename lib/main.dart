import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'lab1.dart';
// import 'lab4.dart';
import 'lab8.dart';
//import 'lab3.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key key}) : super(key: key);
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: 'app_was_opened', parameters: null);
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

var key = new GlobalKey<_MyStatefulWidgetState>();

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);
  //final String title;

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget>
    with WidgetsBindingObserver {
  // int selectedIndex = 0;
  // static List<Widget> _widgetOptions = <Widget>[
  //   AuthApp(),
  //   AuthAppTest(),
  //   Lab1(),
  // ];
  // void _onItemTapped(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //   });
  // }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_was_resumed', parameters: null);
          print("app in resumed!!!!!!!");
          break;
        }
      case AppLifecycleState.inactive:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_is_inactive', parameters: null);
          print("app in inactive!!!!!!!");
          break;
        }
      case AppLifecycleState.paused:
        {
          await FirebaseAnalytics()
              .logEvent(name: 'app_was_paused', parameters: null);
          print("app in paused!!!!!!!");
          break;
        }
      case AppLifecycleState.detached:
        {
          print("app in detached!!!!!!!");
          FirebaseAnalytics()
              .logEvent(name: 'app_was_closed', parameters: null)
              .then((value) => print("LOG WAS SEND!!!!!!!"));
          break;
        }
    }
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Lab8(),
    );
    // bottomNavigationBar: BottomNavigationBar(
    //   items: const <BottomNavigationBarItem>[
    //     BottomNavigationBarItem(
    //       icon: Icon(Icons.perm_identity),
    //       label: 'Профиль',
    //     ),
    //     BottomNavigationBarItem(
    //       icon: Icon(Icons.people_alt_outlined),
    //       label: 'Друзья',
    //     ),
    //     BottomNavigationBarItem(
    //       icon: Icon(Icons.people_alt_outlined),
    //       label: 'Друзья',
    //     ),
    //   ],
    //   currentIndex: _selectedIndex,
    //   selectedItemColor: Colors.blueAccent,
    //   onTap: _onItemTapped,
    // ),
    //);
  }
}
