import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'lab1.dart';
import 'lab2.dart';
import 'lab3.dart';
import 'lab4.dart';
import 'lab6.dart';
import 'lab7.dart';
import 'lab8.dart';

Widget drawer(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text(
            'Список лаб',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
        ),
        ListTile(
            leading: Text("1"),
            title: Text('Lab1'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_1_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab1');
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Lab1()));
            }),
        ListTile(
            leading: Text("2"),
            title: Text('Lab2'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_2_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab2');
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => VideoPlayerApp()));
            }),
        ListTile(
            leading: Text("3"),
            title: Text('Lab3'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_3_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab3');
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Lab3()));
            }),
        ListTile(
            leading: Icon(Icons.person),
            title: Text('Профиль'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_4_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab4');
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => AuthApp()));
            }),
        if (globToken != null)
          ListTile(
              leading: Icon(Icons.people),
              title: Text('Друзья'),
              onTap: () {
                FirebaseAnalytics()
                    .logEvent(name: 'go_to_lab_5_screen', parameters: null);
                // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab5');
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => AuthAppTest()));
              }),
        ListTile(
            leading: Text("6"),
            title: Text('Lab6'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_6_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab6');
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Lab6()));
            }),
        ListTile(
            leading: Text("7"),
            title: Text('Lab7'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_7_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab7');
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Lab7()));
            }),
        ListTile(
            leading: Text("8"),
            title: Text('Lab8'),
            onTap: () {
              FirebaseAnalytics()
                  .logEvent(name: 'go_to_lab_8_screen', parameters: null);
              // FirebaseAnalytics().setCurrentScreen(screenName: 'Lab8');
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Lab8()));
            }),
      ],
    ),
  );
}
