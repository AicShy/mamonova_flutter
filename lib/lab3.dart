import 'package:html/parser.dart' show parse;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';
import 'package:flutter_html/flutter_html.dart';
import 'drawer.dart';

class Lab3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PageView(
      children: <Widget>[
        Container(
          child: Help(),
        ),
        Container(
          child: Scaffold(
            // drawer: drawer(context),
            appBar: AppBar(
              title: Text('ЛР3'),
            ),
            body: SingleChildScrollView(child: Text(htmlData)),
          ),
        ),
        Container(
          child: Scaffold(
            appBar: AppBar(title: Text('ЛР3')),
            body: SingleChildScrollView(child: Html(data: htmlData)),
          ),
        ),
      ],
    );
  }
}

class Help extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HelpState();
  }
}

String data = '';
String htmlData = "";

class HelpState extends State<Help> {
  @override
  void initState() {
    super.initState();
    _loadData();
  }

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _controller.future,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> controller) {
        final bool webViewReady =
            controller.connectionState == ConnectionState.done;
        return Scaffold(
          drawer: drawer(context),
          appBar: AppBar(
            title: Text('ЛР3'),
            actions: <Widget>[
              Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.refresh),
                    onPressed: !webViewReady
                        ? null
                        : () async {
                            await controller.data.clearCache();
                            await controller.data.loadUrl(
                                'https://music.yandex.ru/artist/792433');
                            _loadData();
                          },
                  ),
                ],
              )
            ],
          ),
          body: Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    style: TextStyle(fontSize: 15, color: Colors.black87),
                    textAlign: TextAlign.center,
                    enabled: false,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      labelText: "Этот исполнитель нравится $data людям",
                      focusColor: Colors.blueAccent,
                      fillColor: Colors.white,
                      filled: true,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: double.infinity,
                    child: WebView(
                      initialUrl: 'https://music.yandex.ru/artist/792433',
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller.complete(webViewController);
                        // if (_controller.isCompleted) {}
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          // floatingActionButton: FloatingActionButton(
          //   child: Icon(Icons.refresh),
          //   onPressed: () => _loadData(),
          // ),
        );
      },
    );
  }

  _loadData() async {
    final response = await http.get('https://music.yandex.ru/artist/792433');
    if (response.statusCode == 200) {
      var document = parse(response.body);
      print(document.getElementsByClassName('d-button__label')[0].text);
      print(response.body);
      setState(() {
        htmlData = response.body;
        data = document.getElementsByClassName('d-button__label')[0].text;
      });
    }
  }
}
