import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login_vk/flutter_login_vk.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'drawer.dart';

VKAccessToken globToken;
VKUserProfile globProf;
// String globImg = 'http://vkpng.std-1226.ist.mospolytech.ru/vk.png';
String globImg = '';
int globFriendsCount = 0;
List<dynamic> globFriendsList;

class AuthApp extends StatefulWidget {
  final plugin = VKLogin(debug: true);
  AuthApp({Key key}) : super(key: key);

  @override
  _AuthAppState createState() => _AuthAppState();
}

class _AuthAppState extends State<AuthApp> {
  bool _sdkInitialized = false;
  String _img = 'http://vkpng.std-1226.ist.mospolytech.ru/vk.png';
  String get image => _img;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _initSdk();

    // print(globToken);
    // print(globProf);
    // print(globFriendsCount);
    // print(globFriendsList);
  }

  @override
  Widget build(BuildContext context) {
    // final token = _token;
    // final profile = _profile;
    // final isLogin = token != null;
    //final img = _img;
    //super.build(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: _scaffoldKey,
        drawer: drawer(context),
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
          title: globToken != null
              ? Text('${globProf.firstName} ${globProf.lastName}')
              : Text('Вход'),
          leading: CircleAvatar(
              radius: 25,
              backgroundImage: globImg != ""
                  ? NetworkImage(globImg)
                  : AssetImage('assets/img/vk.png')),
          actions: [
            if (globToken != null)
              IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: _onPressedLogOutButton,
              ),
          ],
        ),
        body: Builder(
          builder: (context) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (globToken != null && globProf != null)
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: _buildUserInfo(context, globProf, globToken),
                    ),
                  ),
                if (globToken == null)
                  OutlinedButton(
                    child: const Text('Вход'),
                    onPressed: () => _onPressedLogInButton(context),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildUserInfo(
      BuildContext context, VKUserProfile profile, VKAccessToken accessToken) {
    final photoUrl = profile.photo200;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        // const Text('User: '),
        Column(
          children: [
            Center(
              child: Text(
                '${profile.firstName} ${profile.lastName}',
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
            ),
            if (photoUrl != null)
              Center(
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    CircleAvatar(
                        radius: 90, backgroundImage: NetworkImage(photoUrl)),
                    profile.onlineMobile
                        ? Positioned(
                            bottom: 3.0,
                            right: 3.0,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Stack(children: <Widget>[
                                  Positioned.fill(
                                    child: Container(
                                      margin: EdgeInsets.all(12),
                                      color: Colors.white, // Color
                                    ),
                                  ),
                                  Icon(
                                    Icons.phone_android,
                                    color: Colors.grey[50],
                                    size: 55.0,
                                  ),
                                ]),
                                Icon(
                                  Icons.phone_android,
                                  color: Colors.green,
                                  size: 45.0,
                                ),
                              ],
                            ),
                          )
                        : Positioned(
                            bottom: 5.0,
                            right: 5.0,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Icon(
                                  Icons.circle,
                                  color: Colors.grey[50],
                                  size: 50.0,
                                ),
                                Icon(
                                  Icons.circle,
                                  color: profile.online
                                      ? Colors.green
                                      : Colors.grey,
                                  size: 40.0,
                                ),
                              ],
                            ),
                          ),
                  ],
                ),
              ),
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          child: Column(
            children: [
              Text('AccessToken'),
              Text(
                accessToken.token,
                softWrap: true,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _onPressedLogInButton(BuildContext context) async {
    final res = await widget.plugin.logIn(scope: [
      VKScope.email,
    ]);

    if (res.isError) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Log In failed: ${res.asError.error}'),
        ),
      );
    } else {
      final loginResult = res.asValue.value;
      if (!loginResult.isCanceled) await _updateLoginInfo();
    }
  }

  Future<void> _onPressedLogOutButton() async {
    await widget.plugin.logOut();
    await _updateLoginInfo();
  }

  Future<void> _initSdk() async {
    await widget.plugin.initSdk('7832060');
    _sdkInitialized = true;
    await _updateLoginInfo();
  }

  Future<void> _updateLoginInfo() async {
    if (!_sdkInitialized) return;

    final plugin = widget.plugin;
    final token = await plugin.accessToken;
    final profileRes = token != null ? await plugin.getUserProfile() : null;
    setState(() {
      // _token = token;
      // _profile = profileRes?.asValue?.value;
      // _email = email;
      globToken = token;
      globProf = profileRes?.asValue?.value;
      if (token != null) {
        globImg = profileRes?.asValue?.value?.photo100;
      } else {
        globImg = '';
        globFriendsCount = 0;
        globFriendsList = null;
      }
    });
  }

  // @override
  // bool get wantKeepAlive => true;
}

class AuthAppTest extends StatefulWidget {
  @override
  _AuthAppTestState createState() => _AuthAppTestState();
}

class _AuthAppTestState extends State<AuthAppTest> {
  //final GlobalKey<ScaffoldState> _scaffoldKey2 = GlobalKey<ScaffoldState>();
  int friendsCount = 0;
  List<dynamic> friendsList;
  VKAccessToken _token;
  VKUserProfile _profile;
  String _img = 'https://pngicon.ru/file/uploads/vk.png';
  bool _isLogin;

  _AuthAppTestState() {
    _token = globToken;
    _profile = globProf;
    _isLogin = globToken != null;
    if (_isLogin) {
      _setFriendsList(_profile, _token);
    }
    // print(globToken);
    // print(globProf);
    // print(globFriendsCount);
    // print(globFriendsList);
  }

  @override
  Widget build(BuildContext context) {
    final isLogin = _isLogin;
    final token = _token;
    final profile = _profile;
    final smolImage = isLogin ? _profile.photo100 : _img;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        //key: _scaffoldKey2,
        drawer: drawer(context),
        appBar: AppBar(
          title: const Text('Список друзей'),
          // leading: TextButton(
          //   child: CircleAvatar(
          //       radius: 25, backgroundImage: NetworkImage(smolImage)),
          //   onPressed: () => _scaffoldKey2.currentState.openDrawer(),
          // ),
          leading: CircleAvatar(
              radius: 25, backgroundImage: NetworkImage(smolImage)),
          actions: [
            if (globToken != null)
              IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () => _setFriendsList(globProf, globToken),
              ),
          ],
        ),
        body: Builder(
          builder: (context) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (token != null && profile != null)
                  Expanded(
                    child: _buildFriendsList(context, profile, token),
                  ),
                if (!isLogin) Text("пожолуйста, войдите")
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _setFriendsList(
      VKUserProfile profile, VKAccessToken accessToken) async {
    if (_isLogin) {
      String uid = profile.userId.toString();
      String token = accessToken.token;
      var response = await http.get(Uri.parse(
          'https://api.vk.com/method/friends.get?user_id=$uid&access_token=$token&v=5.110&fields=photo_100,online,last_seen,status,sex,online_mobile'));
      var friendsListJson = jsonDecode(response.body)['response'];
      setState(() {
        globFriendsCount = friendsListJson['count'];
        globFriendsList = friendsListJson['items'];
        currTab = null;
      });
    }
  }

  int currTab;

  Widget _buildFriendsList(
      BuildContext context, VKUserProfile profile, VKAccessToken accessToken) {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(color: Colors.grey[50]),
        padding: const EdgeInsets.only(right: 8.0, left: 8.0),
        itemCount: globFriendsCount,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Container(
              color: Colors.grey[50],
              child: Center(
                  child: Container(
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: CircleAvatar(
                          radius: 25,
                          backgroundImage: NetworkImage(
                              globFriendsList[index]["photo_100"])),
                    ),
                    currTab == index
                        ? Row(
                            children: [
                              globFriendsList[index]["online"] == 1
                                  ? Text(
                                      "В сети",
                                      style: TextStyle(color: Colors.green),
                                    )
                                  : globFriendsList[index]["sex"] == 1
                                      ? Text(
                                          "Была в сети ${DateFormat('kk:mm dd.MM.y').format(DateTime.fromMillisecondsSinceEpoch(globFriendsList[index]["last_seen"]['time'] * 1000))}",
                                        )
                                      : Text(
                                          "Был в сети ${DateFormat('kk:mm dd.MM.y').format(DateTime.fromMillisecondsSinceEpoch(globFriendsList[index]["last_seen"]['time'] * 1000))}",
                                        )
                            ],
                          )
                        : Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                    "${globFriendsList[index]["first_name"]} ${globFriendsList[index]["last_name"]}"),
                              ),
                              globFriendsList[index]["online_mobile"] == 1
                                  ? Icon(
                                      Icons.phone_android,
                                      color: Colors.green,
                                      size: 15.0,
                                    )
                                  : Icon(
                                      Icons.circle,
                                      color:
                                          globFriendsList[index]["online"] == 1
                                              ? Colors.green
                                              : Colors.grey,
                                      size: 10.0,
                                    )
                            ],
                          ),
                  ],
                ),
              )),
            ),
            onTap: () => {
              setState(() {
                if (currTab != index) {
                  currTab = index;
                } else {
                  currTab = null;
                }
              })
            },
            onDoubleTap: () {
              setState(() {
                currTab = null;
              });
              return _showMyDialog(globFriendsList[index]);
            },
          );
        });
  }

  Future<void> _showMyDialog(Map<String, dynamic> user) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
              child: Text(
            '${user["first_name"]} ${user["last_name"]}',
            softWrap: true,
          )),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Center(
                      child: Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      CircleAvatar(
                          radius: 45,
                          backgroundImage: NetworkImage(user["photo_100"])),
                      user["online_mobile"] == 1
                          ? Positioned(
                              bottom: 3.0,
                              right: 3.0,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Stack(children: <Widget>[
                                    Positioned.fill(
                                      child: Container(
                                        margin: EdgeInsets.all(4),
                                        color: Colors.white,
                                      ),
                                    ),
                                    Icon(
                                      Icons.phone_android,
                                      color: Colors.grey[50],
                                      size: 20.0,
                                    ),
                                  ]),
                                  Icon(
                                    Icons.phone_android,
                                    color: Colors.green,
                                    size: 17.0,
                                  ),
                                ],
                              ),
                            )
                          : Positioned(
                              bottom: 3.0,
                              right: 3.0,
                              child:
                                  Stack(alignment: Alignment.center, children: [
                                Icon(
                                  Icons.circle,
                                  color: Colors.grey[50],
                                  size: 25.0,
                                ),
                                Icon(
                                  Icons.circle,
                                  color: user["online"] == 1
                                      ? Colors.green
                                      : Colors.grey,
                                  size: 20.0,
                                ),
                              ]),
                            ),
                    ],
                  )),
                ),
                if (user["sex"] == 1) Center(child: Text("Пол: Женский")),
                if (user["sex"] == 2) Center(child: Text("Пол: Мужской")),
                if (user["sex"] == 0) Center(child: Text("Пол не указан")),
                user["status"] != ""
                    ? Center(
                        child: Text("Статус: ${user["status"]}"),
                      )
                    : Center(
                        child: Text("Статус не установлен"),
                      )
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Выйти'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
