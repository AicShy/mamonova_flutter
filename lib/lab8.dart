import 'dart:convert';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;

class Lab8 extends StatefulWidget {
  Lab8({Key key}) : super(key: key);
  @override
  _Lab8State createState() => _Lab8State();
}

class _Lab8State extends State<Lab8> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String token = '';
  String time = "";
  String text = "";
  String img = "";

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    Future<void> _showMyDialog(mess) async {
      return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Text(
              "ОЙ!",
              softWrap: true,
            )),
            content: Container(
              child: Text(
                  'Кажется, такого пользователя не существует. Проверьте логин и пароль.'),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Выйти'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    Future<void> _getToken(username, password) async {
      var response = await http.post(
        Uri.parse('https://aicshy.pythonanywhere.com/auth'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, String>{"username": "$username", "password": "$password"},
        ),
      );
      var res = jsonDecode(response.body);
      if (res['status_code'] == 401) {
        setState(() {
          token = '';
          img = '';
          time = '';
          text = '';
        });
        return _showMyDialog('Ой!');
      } else {
        print(res);
        setState(() {
          token = res['access_token'];
          img = '';
          time = '';
          text = '';
        });
      }
    }

    Future<void> _getData() async {
      var response = await http.get(
        Uri.parse('https://aicshy.pythonanywhere.com/protected'),
        headers: <String, String>{
          'Authorization': 'JWT $token',
        },
      );
      var res = jsonDecode(response.body);
      print(res['img'].replaceAll("\n", ""));
      setState(() {
        img = res['img'].replaceAll("\n", "");
        time = res['time'];
        text = res['text'];
      });
    }

    return Scaffold(
      backgroundColor: Colors.grey[50],
      drawer: drawer(context),
      appBar: AppBar(
        title: Text("ЛР8 JWT"),
        leading: Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Image.asset('assets/img/googleBig.png'),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Container(
            height: height - 84,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8, right: 8, top: 12, bottom: 12),
                      child: Container(
                        height: 40,
                        child: Theme(
                          data: ThemeData(
                            primaryColor: Colors.blueAccent,
                          ),
                          child: TextField(
                            controller: _usernameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                ),
                                labelText: 'Логин',
                                focusColor: Colors.blueAccent),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 8, right: 8, bottom: 12),
                      child: Container(
                        height: 40,
                        child: Theme(
                          data: ThemeData(
                            primaryColor: Colors.blueAccent,
                          ),
                          child: TextField(
                            controller: _passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                ),
                                labelText: 'Пароль',
                                focusColor: Colors.blueAccent),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: TextButton(
                            onPressed: () {
                              FirebaseAnalytics().logEvent(
                                  name: 'button_Войти_Lab_8', parameters: null);
                              _getToken(_usernameController.text,
                                  _passwordController.text);
                            },
                            child: Text(
                              'Войти',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w400),
                            ),
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.grey[100]),
                            ),
                          ),
                        ),
                        if (token != "")
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextButton(
                              onPressed: () {
                                FirebaseAnalytics().logEvent(
                                    name: 'button_Получить_данные_Lab_8',
                                    parameters: null);
                                _getData();
                              },
                              child: Text(
                                "Получить данные",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w400),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.grey[100]),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    if (time != '')
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Время сервера на момент запоса: $time'),
                      ),
                    if (text != '')
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('$text'),
                      ),
                    if (img != '')
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Image.memory(base64Decode(img)),
                      ),
                  ],
                ),
                if (token != '')
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text('AccessToken'),
                          Text('$token'),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
