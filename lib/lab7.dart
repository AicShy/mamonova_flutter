import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'drawer.dart';
import 'package:intl/intl.dart';
import 'dart:typed_data';

class Message {
  var text;
  String time;
  bool sender;
  bool isPhoto;

  Message(_text, _time, _sender, _isPhoto) {
    text = _text;
    time = _time;
    sender = _sender;
    isPhoto = _isPhoto;
  }
}

class Lab7 extends StatefulWidget {
  Lab7({Key key}) : super(key: key);
  @override
  _Lab7State createState() => _Lab7State();
}

class _Lab7State extends State<Lab7> {
  @override
  Widget build(BuildContext context) {
    List<Message> loadedMessages = [];
    final TextEditingController _controller = TextEditingController();
    ScrollController _scrollController;
    final _channel = WebSocketChannel.connect(
      Uri.parse('ws://websock.std-1226.ist.mospolytech.ru:80/'),
    );

    void _sendMessage() {
      if (_controller.text.isNotEmpty) {
        _channel.sink.add(_controller.text);
        String now = DateFormat('kk:mm').format(DateTime.now());
        loadedMessages.add(Message(_controller.text, now, true, false));
        _controller.clear();
      }
    }

    return Scaffold(
      backgroundColor: Colors.grey[50],
      drawer: drawer(context),
      appBar: AppBar(
        title: Text("ЛР7 webSock"),
        leading: Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Image.asset('assets/img/googleBig.png'),
        ),
      ),
      body: Stack(
        children: <Widget>[
          StreamBuilder(
              stream: _channel.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var dataList = jsonDecode(snapshot.data);
                  print(dataList);
                  print(dataList['isPhoto']);
                  if (dataList['isPhoto'] == 1) {
                    loadedMessages.add(Message(dataList['content'].cast<int>(),
                        dataList['time'], false, true));
                  } else {
                    loadedMessages.add(Message(
                        dataList['content'], dataList['time'], false, false));
                  }
                }
                return ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height - 140,
                  ),
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: loadedMessages.length,
                    shrinkWrap: false,
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    itemBuilder: (context, index) {
                      if (loadedMessages[index].sender) {
                        return ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(loadedMessages[index].time),
                              Bubble(
                                padding: BubbleEdges.all(10.0),
                                margin: BubbleEdges.only(top: 15.0),
                                nip: BubbleNip.rightBottom,
                                child: Column(children: <Widget>[
                                  Text(loadedMessages[index].text),
                                ]),
                                alignment: Alignment.centerRight,
                                color: Colors.blue.shade200,
                              ),
                            ],
                          ),
                        );
                      } else {
                        return ConstrainedBox(
                          constraints: new BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width - 60,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Bubble(
                                padding: BubbleEdges.all(10.0),
                                margin: BubbleEdges.only(top: 15.0),
                                nip: BubbleNip.leftBottom,
                                child: Column(children: <Widget>[
                                  loadedMessages[index].sender
                                      ? Text(loadedMessages[index].text)
                                      : loadedMessages[index].isPhoto
                                          ? Container(
                                              height: 200,
                                              width: 200,
                                              child: Image.memory(
                                                  Uint8List.fromList(
                                                      loadedMessages[index]
                                                          .text)),
                                            )
                                          : Text(loadedMessages[index].text),
                                ]),
                                alignment: Alignment.centerLeft,
                                color: Colors.grey.shade200,
                              ),
                              Text(loadedMessages[index].time),
                            ],
                          ),
                        );
                      }
                    },
                  ),
                );
              }),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
              height: 60,
              width: double.infinity,
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      onSubmitted: (String str) => _sendMessage(),
                      controller: _controller,
                      decoration: InputDecoration(
                          // suffixIcon: IconButton(
                          //   onPressed: () => _sendMessage(),
                          //   icon: Icon(
                          //     Icons.send,
                          //     color: Colors.blue,
                          //     size: 25,
                          //   ),
                          // ),
                          hintText: "Введите сообщение",
                          hintStyle: TextStyle(color: Colors.black54),
                          border: InputBorder.none),
                    ),
                  ),
                  FloatingActionButton(
                    backgroundColor: Colors.white,
                    elevation: 0,
                    focusElevation: 0,
                    hoverElevation: 0,
                    highlightElevation: 0,
                    disabledElevation: 0,
                    onPressed: () => _sendMessage(),
                    child: Icon(
                      Icons.send,
                      color: Colors.blue,
                      size: 25,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
