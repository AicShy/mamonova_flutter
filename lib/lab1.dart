import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infinite_listview/infinite_listview.dart';
import 'drawer.dart';

class Lab1 extends StatefulWidget {
  Lab1({Key key}) : super(key: key);
  @override
  _Lab1State createState() => _Lab1State();
}

class _Lab1State extends State<Lab1> {
  double _currentSliderValue = 20;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.grey[50],
      drawer: drawer(context),
      appBar: AppBar(
        title: Text("ЛР1 Элементы GUI"),
        leading: Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Image.asset('assets/img/googleBig.png'),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: SingleChildScrollView(
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset('assets/img/googleBig.png'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: Container(
                      height: 40,
                      child: Theme(
                        data: ThemeData(
                          primaryColor: Colors.blueAccent,
                        ),
                        child: TextField(
                            decoration: InputDecoration(
                          suffixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ),
                          labelText: 'Поиск',
                        )),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: TextButton(
                          onPressed: () {
                            FirebaseAnalytics().logEvent(
                                name: 'button_Поиск_в_Google',
                                parameters: null);
                          },
                          child: Text(
                            'Поиск в Google',
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w400),
                          ),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey[100]),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            'Мне повезет!',
                            style: TextStyle(
                                color: Colors.black87,
                                fontWeight: FontWeight.w400),
                          ),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey[100]),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 8),
                            child: Text('Введите логин'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 12),
                            child: Container(
                              height: 30,
                              width: width / 2 - 16,
                              child: Theme(
                                data: ThemeData(
                                  primaryColor: Colors.blueAccent,
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                      ),
                                      labelText: 'Логин',
                                      focusColor: Colors.blueAccent),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 8),
                            child: Text('Введите E-mail'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 12),
                            child: Container(
                              height: 30,
                              width: width / 2 - 16,
                              child: Theme(
                                data: ThemeData(
                                  primaryColor: Colors.blueAccent,
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                      ),
                                      labelText: 'E-mail',
                                      focusColor: Colors.blueAccent),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 8),
                            child: Text('Введите пароль'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 12),
                            child: Container(
                              height: 30,
                              width: width / 2 - 16,
                              child: Theme(
                                data: ThemeData(
                                  primaryColor: Colors.blueAccent,
                                ),
                                child: TextField(
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                      ),
                                      labelText: 'Пароль',
                                      focusColor: Colors.blueAccent),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8, right: 8, bottom: 8),
                            child: Text('Повторите пароль'),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            child: Container(
                              height: 30,
                              width: width / 2 - 16,
                              child: Theme(
                                data: ThemeData(
                                  primaryColor: Colors.blueAccent,
                                ),
                                child: TextField(
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                      ),
                                      labelText: 'Повторите пароль',
                                      focusColor: Colors.blueAccent),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Column(children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8, right: 8, bottom: 8),
                              child: Text('Ввеедите время'),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12),
                              child: Row(
                                children: [
                                  Container(
                                    height: 125,
                                    width: (width / 2) / 3 - 20,
                                    child: InfiniteListView.builder(
                                      itemBuilder: (context, int index) {
                                        return Center(
                                            child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('${index % 24}'),
                                        ));
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 10,
                                    child: Text(":"),
                                  ),
                                  Container(
                                    height: 125,
                                    width: (width / 2) / 3 - 20,
                                    child: InfiniteListView.builder(
                                      itemBuilder: (context, int index) {
                                        return Center(
                                            child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('${index % 60}'),
                                        ));
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 10,
                                    child: Text(":"),
                                  ),
                                  Container(
                                    height: 125,
                                    width: (width / 2) / 3 - 20,
                                    child: InfiniteListView.builder(
                                      itemBuilder: (context, int index) {
                                        return Center(
                                            child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text('${index % 60}'),
                                        ));
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 8, right: 8, bottom: 8),
                              child: Text('Ввеедите громкость'),
                            ),
                            Container(
                              width: (width / 2) - 24,
                              child: Slider(
                                  value: _currentSliderValue,
                                  min: 0,
                                  max: 100,
                                  onChanged: (double value) {
                                    setState(() {
                                      _currentSliderValue = value;
                                    });
                                  }),
                            ),
                          ],
                        )
                      ])
                    ],
                  )
                ],
              ))),
        ),
      ),
    );
  }
}
